# README #
EHDViz | Data Visualization Framework for Integrated Health Assessment

This repository contains the source code for several demonstrations of an interactive, real-time, health data visualization tool featured at [ehdviz.dudleylab.org](http://ehdviz.dudleylab.org).  The examples are intended for disparate contexts and use diverse sources of health information (EMR data, wearable health sensors, personal health logs) to exemplify the versatility of this framework.  It was developed using the opensource language R for all operations including data processing, web scraping, web app user interface/server development, and web app deployment.

Each folder at the top level of the repository hierarchy corresponds to one web application.  Each web app folder contains 1) server.R, 2) user.R, and 3) offspring folders containing images and data inputs called by the web app.  Although initial development was performed using both inpatient and outpatient cohorts from mount sinai hospital, we've developed simulated data and mined our own wearable monitor data to allow you to run these demonstrations.

### Set Up ###
1) Download the folder for the app you'd like to run  
2) Install R and the package dependencies.  The application was developed with the R session info found [here](https://bitbucket.org/dudleylab/ehdviz/src/master/sessionInfo.txt).

```
#!R

install.packages(c("reshape2","gridExtra","ggplot2","shiny","fitbitScraper"))
```

3) In R, change the working directory to the location of the app folder (foo) and enter:
```
#!R
library(shiny)
runApp("foo")
```
Each app automatically calls the data dependencies contained in subfolders and runs in a browser.  To add additional sources of health data, make an input file formatted as shown in [the help page](http://143.95.94.12/EHDviz/help.html) and change the file input line, or add additional parallel panels as demonstrated in the quantified self implementation.

### Contact Us ###
marcus.badgeley@mssm.edu
Dudley Lab | http://dudleylab.org/